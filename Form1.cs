﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Модульна_Контрольна_Робота
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }


        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстові файли (*.txt)|*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                int positiveCount = 0;
                int negativeCount = 0;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string[] lines = File.ReadAllLines(filePath);
                    foreach (string line in lines)
                    {
                        if (double.TryParse(line, out double number))
                        {
                            if (number > 0)
                                positiveCount++;
                            else if (number < 0)
                                negativeCount++;
                        }
                    }

                    positiveCountLabel.Text = "Кількість додатніх чисел: " + positiveCount;
                    negativeCountLabel.Text = "Кількість від'ємних чисел: " + negativeCount;
                }
                else
                {
                    MessageBox.Show("Файл не знайдено.");
                }

              
            }
        }
    }
}
