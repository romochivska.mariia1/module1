﻿namespace Модульна_Контрольна_Робота
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.positiveCountLabel = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.negativeCountLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // positiveCountLabel
            // 
            this.positiveCountLabel.AutoSize = true;
            this.positiveCountLabel.Location = new System.Drawing.Point(12, 75);
            this.positiveCountLabel.Name = "positiveCountLabel";
            this.positiveCountLabel.Size = new System.Drawing.Size(44, 16);
            this.positiveCountLabel.TabIndex = 1;
            this.positiveCountLabel.Text = "label1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 33);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Відкрити файл";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // negativeCountLabel
            // 
            this.negativeCountLabel.AutoSize = true;
            this.negativeCountLabel.Location = new System.Drawing.Point(12, 101);
            this.negativeCountLabel.Name = "negativeCountLabel";
            this.negativeCountLabel.Size = new System.Drawing.Size(44, 16);
            this.negativeCountLabel.TabIndex = 3;
            this.negativeCountLabel.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 206);
            this.Controls.Add(this.negativeCountLabel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.positiveCountLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label positiveCountLabel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label negativeCountLabel;
    }
}

